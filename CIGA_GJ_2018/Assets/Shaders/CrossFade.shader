﻿Shader "Custom/CrossFade"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_SecondTex("Second Texture", 2D) = "white" {}
		_Tween("Tween", Range(0, 1)) = 0
	}
	SubShader
	{
		Tags
		{
			"RenderType"="Opaque"
			"Queue" = "Transparent"
		}
		LOD 100

		Pass
		{
			//list of blend modes: https://docs.unity3d.com/Manual/SL-Blend.html
			Blend SrcAlpha OneMinusSrcAlpha //define how each pixel will blend with the pixel beneath it on the screen

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			//there are four key parts of the shader

			struct appdata //defines what information we are getting from each vertex from the mesh
			{
				float4 vertex : POSITION;
				float4 uv : TEXCOORD0;
			};

			struct v2f //what information we are passing into the fragment function
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0; //pass along uv data to the individual pixels
			};

			v2f vert(appdata v) //all it looks on the mesh is the position of the vertex, typically Vector3 in the mesh's local coordinate system.
			{
				v2f o; //initialize a v2f called o
				o.vertex = UnityObjectToClipPos(v.vertex); //take it from a point relative to the object, and transform it into a point on a screen
				o.uv = v.uv;
				return o;
			}

			sampler2D _MainTex;
			sampler2D _SecondTex;
			float _Tween;

			float4 frag(v2f i) : SV_Target //takes v2f struct and returns a color
			{
				//position is locked in by the vert function, just change the color at that given position
				float4 uv_color = float4(i.uv.x, i.uv.y, 1, 1);
				float4 color = (1.0f - _Tween) * tex2D(_MainTex, i.uv) + _Tween * tex2D(_SecondTex, i.uv); //get the color value from the MainTex at the provided UV value
				return color;
			}
			ENDCG
		}
	}
}
