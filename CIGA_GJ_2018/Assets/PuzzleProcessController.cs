﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleProcessController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        this.GetComponent<Text>().text = GlobalVariables.puzzleCompletion[GameObject.Find("MenuCanvas").GetComponent<GameMaster>().currentPuzzle - 1].ToString();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setPuzzleProcess(int storyNum)
    {
        print("game process: " + GlobalVariables.gameProgress2.Values.Count(v => v != 0).ToString());
        if (storyNum == 0)
        {
            GlobalVariables.puzzleCompletion[storyNum] = GlobalVariables.gameProgress1.Values.Count(v => v != 0);
        }
        else
        {
            GlobalVariables.puzzleCompletion[storyNum] = GlobalVariables.gameProgress2.Values.Count(v => v != 0);
        }
        GameObject.Find("FinishedNum").GetComponent<Text>().text = GlobalVariables.puzzleCompletion[storyNum].ToString();
    }
}
