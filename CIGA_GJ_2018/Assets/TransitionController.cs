﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransitionController : MonoBehaviour {

    private int currentChapter;
    public Sprite[] transitionImgs = new Sprite[3];
    public Sprite trueEndTransitionSprite;

	// Use this for initialization
	void Start () {
        currentChapter = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void incrementChapter()
    {
        currentChapter += 1;
        this.GetComponent<Image>().sprite = transitionImgs[currentChapter % 3];
    }

    public void unlockTrueEndTransition()
    {
        this.GetComponent<Image>().sprite = trueEndTransitionSprite;
    }
}
