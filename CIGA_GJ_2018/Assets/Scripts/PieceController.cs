﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class PieceController : MonoBehaviour,IBeginDragHandler, IDragHandler, IEndDragHandler {

 
    public Vector2 initialPos;
    public Vector2 posOnUpdate;
    public GameObject confirmButton;
    HashSet<Vector2Int> movingUnitPositions=new HashSet<Vector2Int>();

    //post-ciga
    public PuzzleConfirm puzzleConfirm;

    void Start()
    {
        puzzleConfirm = GameObject.Find("Submit").GetComponent<PuzzleConfirm>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!puzzleConfirm.WaitForContinue)
        {
            //initialPos = eventData.position;
            initialPos = this.transform.position;
            posOnUpdate = this.transform.position;
            movingUnitPositions.Clear();
            int pieceid = GetComponent<PieceProperty>().id;
            AddMovingUnits(pieceid + 1);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!puzzleConfirm.WaitForContinue)
        {
            //Debug.Log(Camera.main.ScreenToWorldPoint(eventData.position)+"ong drag");
            this.transform.position = eventData.position;

            //Debug.Log(GlobalVariables.pieceobjects[15].GetComponent<PieceProperty>().pos);
            //Debug.Log(GlobalVariables.pieceobjects[16].GetComponent<PieceProperty>().pos);
            int pieceid = GetComponent<PieceProperty>().id;
            transformUnit(pieceid + 1, posOnUpdate.x, posOnUpdate.y, eventData.position.x, eventData.position.y);
            posOnUpdate = eventData.position;

            //Debug.Log(eventData.position+"drag begin");
        }
    }

    public void OnEndDrag(PointerEventData eventData)

    {
;       if (!puzzleConfirm.WaitForContinue)
        {
            if (GlobalVariables.debugMode && GlobalVariables.firstrun)
            {
                Debug.Log("debug mode");
                Debug.Log(GlobalVariables.pieceobjects.Length);
                Debug.Log(GlobalVariables.pieceobjects[0]);
                autoSnapDebugMode();
                GlobalVariables.firstrun = false;
            }
            else
            {

                Debug.Log("OnEndDrag" + GetComponent<PieceProperty>().id);
                //Debug.Log(Camera.main.ScreenToWorldPoint(eventData.position) + "end drag position");
                int pieceid = GetComponent<PieceProperty>().id;
                bool snapsucess = Snap(eventData.position, pieceid);
                if (snapsucess)
                {
                    GameObject.Find("Submit").GetComponent<PuzzleConfirm>().CheckPuzzle(pieceid);
                    //confirmButton.onClick.Invoke();

                }
            }
        }

        
        //Debug.Log(snapTo);
    }

    public Vector2 closestPoint(Vector2[] points,Vector2 mypoint){
        float mindist = float.MaxValue;
        Vector2 closest = new Vector2(0,0);
        for (int i = 0; i < points.Length;i++){
            Vector2 curr = points[i];
            if((mypoint-curr).magnitude<mindist){
                closest = curr;
                mindist = (mypoint - curr).magnitude;

            }
        }
        return closest;
    }
    public void autoSnapDebugMode(){
        for (int i = 0; i < GlobalVariables.gridHeight;i++){
            for (int j = 0; j < GlobalVariables.gridWidth; j++)
            {
                int num = i * GlobalVariables.gridWidth + j;
                Snap(GlobalVariables.grid[j][i].position,num);

            }
        }
    }
    public bool posContainedInMovingUnit(Vector2Int pos){
        Debug.Log("checking moving unit");
        foreach(Vector2Int mypos in movingUnitPositions){
            Debug.Log(mypos+"   ---"+pos);
            if((mypos-pos).magnitude<0.0001){
                return true;
            }

        }
        return false;
    }
    public bool Snap(Vector2 position,int pieceid){
        Vector2 worldPosition = position;
        Vector2Int snapToPos = closestPoint(GlobalVariables.grid, worldPosition);
        Vector2 snapTo = GlobalVariables.grid[snapToPos.x][snapToPos.y].position;

        GameObject thisgameobject = GlobalVariables.pieceobjects[pieceid];

        //Debug.Log(GlobalVariables.pieceobjects[29].GetComponent<PieceProperty>().pos+"@"+pieceid);
        if ((worldPosition - snapTo).magnitude < 80)
        {
            //posContainedInMovingUnit(snapToPos)
            if (!GlobalVariables.grid[snapToPos.x][snapToPos.y].isOccupied||posContainedInMovingUnit(snapToPos))
            {
                //snap success
                Vector2Int oldpos = thisgameobject.GetComponent<PieceProperty>().pos;
                //Debug.Log(oldpos);

                GlobalVariables.pieceobjects[pieceid].transform.position = snapTo;
                //Debug.Log("snap To" + snapToPos);
                bool movesuccess=moveUnit(GetComponent<PieceProperty>().id + 1, oldpos.x, oldpos.y, snapToPos.x, snapToPos.y);

                //Debug.Log(movesuccess);
                if (movesuccess)
                {
                    if (oldpos.x >= 0 && oldpos.y >= 0&&GlobalVariables.grid[oldpos.x][oldpos.y].pieceid==pieceid)
                    {
                        //Debug.Log("----------------" + oldpos+" "+pieceid);
                       
                        GlobalVariables.grid[oldpos.x][oldpos.y].isOccupied = false;
                        GlobalVariables.grid[oldpos.x][oldpos.y].pieceid = -1;
                    }
                    GlobalVariables.grid[snapToPos.x][snapToPos.y].isOccupied = true;
                    GlobalVariables.grid[snapToPos.x][snapToPos.y].pieceid = pieceid;
                    //Debug.Log("autosnap" + snapToPos+" "+pieceid);
                    thisgameobject.GetComponent<PieceProperty>().pos = snapToPos;


                    //check puzzle success
                    //;

                    return true;
                }
                else{
                    //reset unit's position
                    Debug.Log("move faileddd");
                    this.transform.position = initialPos;
                }



            }
            else
            {
                //this.transform.position = initialPos;
                Vector2Int oldpos = thisgameobject.GetComponent<PieceProperty>().pos;
                if (oldpos.x >= 0 && oldpos.y >= 0)
                {
                    this.transform.position = GlobalVariables.grid[oldpos.x][oldpos.y].position;
                    //GlobalVariables.grid[oldpos.x][oldpos.y].isOccupied = false;
                    //GlobalVariables.grid[oldpos.x][oldpos.y].pieceid = -1;
                }
                else{
                    this.transform.position = initialPos;
                }
            }
        }
        else
        {
            Debug.Log("did not snap"+pieceid+" "+containsUnit(pieceid + 1));
            Vector2Int oldpos = thisgameobject.GetComponent<PieceProperty>().pos;
         

            if (containsUnit(pieceid + 1))
            {
                Debug.Log("contains unit");
               
                this.transform.position = initialPos;
                if (oldpos.x >= 0 && oldpos.y >= 0)
                {
                    this.transform.position = GlobalVariables.grid[oldpos.x][oldpos.y].position;

                }
                
            }
            else{
                if (oldpos.x >= 0 && oldpos.y >= 0)
                {
                    Debug.Log("else");

                    GlobalVariables.grid[oldpos.x][oldpos.y].isOccupied = false;
                    GlobalVariables.grid[oldpos.x][oldpos.y].pieceid = -1;
                    GetComponent<PieceProperty>().pos = new Vector2Int(-1, -1);
                    GlobalVariables.pieceobjects[pieceid].GetComponent<PieceProperty>().pos = new Vector2Int(-1, -1);
                }
            }
        }

        resetUnitPosition(pieceid + 1);
        return false;
        
    }

    public void resetUnitPosition(int spritenum){
        foreach (Unit u in GlobalVariables.units)
        {
            if (u.spritenums.Contains(spritenum))
            {
                //Debug.Log("#7-2" + GlobalVariables.pieceobjects[6].GetComponent<PieceProperty>().pos);

                u.resetPos();


            }
        }
    }
    public bool moveUnit(int spritenum,int oldx,int oldy,int newx,int newy){
        
        foreach (Unit u in GlobalVariables.units){
            if(u.spritenums.Contains(spritenum)){
                //Debug.Log("#7-2" + GlobalVariables.pieceobjects[6].GetComponent<PieceProperty>().pos);
                bool success=u.Move(spritenum,oldx, oldy, newx, newy);
                if(!success){
                    
                    return false;
                }
                Debug.Log("moved");

            }
        }
        return true;
    }
    //temporarily transofrm unit to match the moving piece
    public void transformUnit(int spritenum, float oldx, float oldy, float newx, float newy)
    {

        foreach (Unit u in GlobalVariables.units)
        {
            if (u.spritenums.Contains(spritenum))
            {
                Vector2Int currpos = GlobalVariables.pieceobjects[spritenum - 1].GetComponent<PieceProperty>().pos;
                u.tranformUnit(spritenum, oldx, oldy, newx, newy);



            }
        }
      
    }
    public void AddMovingUnits(int spritenum){
        foreach (Unit u in GlobalVariables.units)
        {
            if (u.spritenums.Contains(spritenum))
            {
                foreach(int x in u.spritenums)
                {
                    Vector2Int currpos = GlobalVariables.pieceobjects[x - 1].GetComponent<PieceProperty>().pos;
                    movingUnitPositions.Add(currpos);
                    Debug.Log("Add pos" + currpos + "num" + spritenum);
                }
            }
        }
    }
    public bool containsUnit(int spritenum){
        foreach (Unit u in GlobalVariables.units)
        {
            if (u.spritenums.Contains(spritenum))
            {
                return true;
            }
        }
        return false;
    }
    public Vector2Int closestPoint(Cell[][] grid, Vector2 mypoint)
    {
        float mindist = float.MaxValue;
        Vector2 closest = new Vector2(0, 0);
        Vector2Int closestpos = new Vector2Int(0, 0);

        for (int i = 0; i < grid.Length; i++)
        {
            for (int j = 0; j < grid[i].Length; j++)
            {
                Vector2 curr = grid[i][j].position;
                if ((mypoint - curr).magnitude < mindist)
                {
                    closest = curr;
                    mindist = (mypoint - curr).magnitude;
                    closestpos = new Vector2Int(i, j);
                }
            }
        }

        return closestpos;
    }

}
