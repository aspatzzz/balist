﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceProperty : MonoBehaviour {

    public int id;
    public Vector2Int pos=new Vector2Int(-1,-1);
    public int objid;
    public int partnum;
    public Vector2Int partRelativePos;
    public bool canflipHorizontal=false;
    public bool canflipVertical=false;

	private void Awake()
	{
        pos = new Vector2Int(-1, -1);
	}
	

    public PieceProperty(int myid,int myobjid,int mypartnum,Vector2Int myPartRelativePos){
        id = myid;
        objid = myobjid;
        partnum = mypartnum;
        partRelativePos = myPartRelativePos;
    }

    public void setIds(int myobjid, int mypartnum, int rpx,int rpy){
        objid = myobjid;
        partnum = mypartnum;
        partRelativePos = new Vector2Int(rpx,rpy);
    }


}
