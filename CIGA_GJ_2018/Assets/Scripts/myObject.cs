﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myObject {

    public int objid;

    public HashSet<Vector2Int> possibleLocations=new HashSet<Vector2Int>();
    public Dictionary<int, Vector2> relpos=new Dictionary<int,Vector2>();//partnum,relative position
    public Dictionary<int, int> pieces=new Dictionary<int, int>();//partnum,spritenum
    public string myname;
    public Dictionary<int,Vector2Int> pieceList = new Dictionary<int,Vector2Int>();//index by sprite number
    public Dictionary<int, int> piecepartnumList = new Dictionary<int, int>();//sprite number,part num 
    public bool triggersAnimation = false;
    public bool isunit = true;
    public Dictionary<int, string> spriteList = new Dictionary<int, string>();//animation texture name
    public void addLocation(Vector2Int loc){
        possibleLocations.Add(loc);
        
    }
    public void addPiece(int partnum, int spritenum,Vector2Int relpos)
    {
        this.relpos.Add(partnum, relpos);
        this.pieces.Add(partnum, spritenum);
        this.pieceList.Add(spritenum, relpos);
        this.piecepartnumList.Add(spritenum, partnum);
    }

    public void addPiece(int partnum, int spritenum, int relposx,int relposy)
    {
        this.relpos.Add(partnum, new Vector2Int(relposx,relposy));
        this.pieces.Add(partnum, spritenum);
        this.pieceList.Add(spritenum,new Vector2Int(relposx, relposy));
        this.piecepartnumList.Add(spritenum, partnum);
        isunit = true;

    }

    public void addPiece(int partnum, int spritenum, int relposx, int relposy, string spriteName)
    {
        this.relpos.Add(partnum, new Vector2Int(relposx, relposy));
        this.pieces.Add(partnum, spritenum);
        this.pieceList.Add(spritenum, new Vector2Int(relposx, relposy));
        this.piecepartnumList.Add(spritenum, partnum);
        this.spriteList.Add(spritenum, spriteName);
        triggersAnimation = true;
        isunit = false;

    }
    public void addName(string n){
        this.myname = n;
    }
  
}
