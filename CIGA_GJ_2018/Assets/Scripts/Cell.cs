﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell{
    public Vector2 position=new Vector2Int(-1, -1);
    public bool isOccupied=false;
    public int pieceid = -1;
    public Cell(){
        
    }
    public void setPosition(Vector2 pos){
        this.position = pos;
    }
}
