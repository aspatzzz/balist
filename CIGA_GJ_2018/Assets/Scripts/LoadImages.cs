﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadImages : MonoBehaviour {

    // Use this for initialization
    public GameObject imagepiece;
    public GameObject emptytile;
    public GameObject confirmButton;
    public GameObject canvas;
    public GameObject scrollText;
    public Material myShader;

	void Start () {
        //Debug.Log(GlobalVariables.gametree2[",1,0,0"]);
        //Debug.Log(GlobalVariables.gametree2.Count);
        //GameObject.Find("PuzzlesHolder").transform.GetChild(1).GetComponent<PuzzleSelector>().UnlockPuzzle();

        //.GetChild(1).GetComponent<PuzzleSelector>().UnlockPuzzle();
        Debug.Log("hi");

        //GameObject.Find("Submit").GetComponent<PuzzleConfirm>().UnlockSetup(1);
        for (int i = 0; i < GlobalVariables.gridWidth; i++)
        {
            for (int j = 0; j < GlobalVariables.gridHeight; j++)
            {

                float xpos = GlobalVariables.gridStart.x + i * GlobalVariables.cellWidth;
                float ypos = GlobalVariables.gridStart.y - j * GlobalVariables.cellHeight;
                var createEmpty = Instantiate(emptytile, new Vector2(xpos, ypos), transform.rotation) as GameObject;
                createEmpty.transform.SetParent(canvas.transform, false);
                GlobalVariables.grid[i][j].position = createEmpty.transform.position;

            }
        }
        Debug.Log(GlobalVariables.grid[0][0].position+"my transform position");
        //var createImage = Instantiate(imagepiece, new Vector2(-50, -150), transform.rotation) as GameObject;
        //createImage.transform.SetParent(canvas.transform, false);
        //createImage.gameObject.GetComponent<Image>().sprite = Resources.Load("image_part_002", typeof(Sprite)) as Sprite;
        //createImage.gameObject.GetComponent<PieceProperty>().id = 0;
        //createImage.gameObject.GetComponent<PieceProperty>().setIds(0, 0, 0, 0);
        //GlobalVariables.pieceobjects[0] = createImage.gameObject;

        //createImage = Instantiate(imagepiece, new Vector2(-250, -150), transform.rotation) as GameObject;
        //createImage.transform.SetParent(canvas.transform, false);
        //createImage.gameObject.GetComponent< PieceProperty > ().id=1;
        //createImage.gameObject.GetComponent<PieceProperty>().setIds(0, 1, 0, 1);
        //GlobalVariables.pieceobjects[1] = createImage.gameObject;

        //createImage = Instantiate(imagepiece, new Vector2(50, -150), transform.rotation) as GameObject;
        //createImage.transform.SetParent(canvas.transform, false);
        //createImage.gameObject.GetComponent< PieceProperty > ().id=2;
        //createImage.gameObject.GetComponent<PieceProperty>().setIds(1, 0, 0, 0);
        //GlobalVariables.pieceobjects[2] = createImage.gameObject;
        int storyno = GameObject.Find("MenuCanvas").GetComponent<GameMaster>().currentPuzzle - 1;

        initObjects(storyno);
        //initObjects(1);
        loadPuzzleImage(storyno);
        Debug.Log(storyno + "storyno");
  
  
        //var createButton = Instantiate(confirmButton, GlobalVariables.buttonStart, transform.rotation) as GameObject;
        //createButton.transform.SetParent(canvas.transform, false);


        //var createScrollText = Instantiate(scrollText, GlobalVariables.textStart, transform.rotation) as GameObject;
        //createScrollText.transform.SetParent(canvas.transform, false);
        //Debug.Log(scrollText.GetComponentInChildren<ScrollRect>().content.GetComponent<Text>().text);
        //Debug.Log(GlobalVariables.readFile("0"));
        //scrollText.GetComponentInChildren<ScrollRect>().content.GetComponent<Text>().text =  GlobalVariables.readFile("3c2");
        //Sprite myspirte = Resources.Load("image_part_001", typeof(Sprite)) as Sprite;
        //Sprite myspirte2 = Resources.Load("image_part_002", typeof(Sprite)) as Sprite;
        //myShader.EnableKeyword("_NORMALMAP");
        //myShader.SetTexture("_Texture", myspirte.texture);

        //myShader.SetFloat("_Tween", 0.8f);
        //myShader.Lerp(Resources.Load("img1") as Material, Resources.Load("img2") as Material, 0.8f);;
      
		
	}
    public void setScrollText(){
        
    }
    public void loadPuzzleImage(int storyno){

        string filestem = "";
         if(storyno==0){
            filestem = "image_part_";
         }
        else if(storyno==1){
            filestem = "Puzzle2/images/image_part_";
        }
        else if (storyno == 2)
        {
            filestem = "Puzzle3/images/image_part_";
        }

        List<int> randomList = new List<int>();

        int count = 0;
        //while(count<30){
            
        //    int x = Random.Range(-1, 29);
           
        //    if (!randomList.Contains(x) &&x<30&&x>=0)
        //    {
        //        Debug.Log(x);
        //        randomList.Add(x);
        //        count++;
        //    }
        //}
        int[] order = randomList.ToArray();
        order=new int[]{7, 8, 12, 23, 21, 6, 29, 18, 28, 1, 15, 16, 20, 11, 30, 24, 14, 4, 19, 13, 26, 25, 27, 10, 5, 3, 22, 2, 17, 9};
        //order = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 };
        order = shuffleArr(order);
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                int num = i * 5 + j + 1;
                Vector2 position = new Vector2(0, 0);
                if (num <= 15)
                {
                    int myx = (num-1) / 3;
                    int myy = (num-1) % 3;
                    position = new Vector2(232, 108) - new Vector2(-myy * GlobalVariables.cellWidth, myx * GlobalVariables.cellHeight);

                }
                else{
                    int myx = (num-16) / 3;
                    int myy = (num-16) % 3;
                    position = new Vector2(-312, 108) - new Vector2(-myy * GlobalVariables.cellWidth, myx * GlobalVariables.cellHeight);

                }
                var createImage = Instantiate(imagepiece, position, transform.rotation) as GameObject;
                createImage.transform.SetParent(canvas.transform, false);
                int imagenum = order[num-1];
                if (imagenum < 10)
                {
                    createImage.gameObject.GetComponent<Image>().sprite = Resources.Load(filestem+"00" + imagenum, typeof(Sprite)) as Sprite;
                }
                else
                {
                    createImage.gameObject.GetComponent<Image>().sprite = Resources.Load(filestem +"0" + imagenum, typeof(Sprite)) as Sprite;
                }
                createImage.gameObject.GetComponent<PieceProperty>().id = imagenum - 1;

                //the textrues are placeholder
                Sprite myspirte = Resources.Load("Replace animations/BallorPotatoFalling16", typeof(Sprite)) as Sprite;
                Sprite myspirte2 = Resources.Load("image_part_002", typeof(Sprite)) as Sprite;
                myShader.EnableKeyword("_NORMALMAP");
                if (i < 3)
                {
                    myShader.SetTexture("_SecondTex", myspirte.texture);
                }
                else{
                    myShader.SetTexture("_SecondTex", myspirte2.texture);
                }
                createImage.gameObject.GetComponent<Image>().material = new Material(myShader);
                myShader.SetFloat("_Tween", 0f);
                //placeholder testing code end

                //createImage.gameObject.GetComponent<PieceProperty>().setIds(1, 0, 0, 0);
                setIDfromObject(createImage, imagenum,storyno);
                GlobalVariables.pieceobjects[imagenum - 1] = createImage.gameObject;



            }
        }
    }

    /* Knuff shuffle algorithm */
    int[] shuffleArr(int[] order)
    {
        for (int i = 0; i < order.Length; i++)
        {
            int tmp = order[i];
            int randIdx = Random.Range(i, order.Length);
            order[i] = order[randIdx];
            order[randIdx] = tmp;
        }
        return order;
    }

    public static Texture2D textureFromSprite(Sprite sprite)
    {
        if (sprite.rect.width != sprite.texture.width)
        {
            Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
            Color[] newColors = sprite.texture.GetPixels((int)sprite.textureRect.x,
                                                         (int)sprite.textureRect.y,
                                                         (int)sprite.textureRect.width,
                                                         (int)sprite.textureRect.height);
            newText.SetPixels(newColors);
            newText.Apply();
            return newText;
        }
        else
            return sprite.texture;
    }
   public void setIDfromObject(GameObject createImage,int spritenum,int storyno){
        myObject[] myObjects=GlobalVariables.myobjects;
        if(storyno==0){
            myObjects = GlobalVariables.myobjects;
        }
        if (storyno == 1)
        {
            myObjects = GlobalVariables.myobjects;
        }


        for (int i = 0; i < myObjects.Length;i++){
            myObject currObject = myObjects[i];
     
            if(currObject!=null &&currObject.pieceList.ContainsKey(spritenum)){
                createImage.gameObject.GetComponent<PieceProperty>().setIds(i,currObject.piecepartnumList[spritenum] , currObject.pieceList[spritenum].x, currObject.pieceList[spritenum].y);
                //Debug.Log(i+" "+ currObject.piecepartnumList[spritenum]+" "+ currObject.pieceList[spritenum].x+" "+currObject.pieceList[spritenum].y);
            }
        }

    }
    void initObjects(int mapnum){
        //NOTE
        //assumption:
        // each myobject object ocntains unqiue id
        // myobjects2 may have objects sharing the same id;
        if (mapnum == 0)
        {
            GlobalVariables.myobjects = new myObject[12];
            GlobalVariables.myobjects[0] = new myObject();
            GlobalVariables.myobjects[0].addLocation(new Vector2Int(0,0));
            GlobalVariables.myobjects[0].addPiece(0, 1, 0, 0);
            GlobalVariables.myobjects[0].addPiece(1, 3, 2, 0);
            GlobalVariables.myobjects[0].addPiece(2, 5, 4, 0);
            GlobalVariables.myobjects[0].addPiece(3, 6, 0, 1);
            GlobalVariables.myobjects[0].addPiece(4, 8, 2, 1);
            GlobalVariables.myobjects[0].addPiece(5, 10, 4, 1);
            GlobalVariables.myobjects[0].addPiece(6, 11, 0, 2);
            GlobalVariables.myobjects[0].addPiece(7, 14, 3, 2);
            GlobalVariables.myobjects[0].addPiece(8, 15, 4, 2);
            GlobalVariables.myobjects[0].addPiece(9, 18, 2, 3);
            GlobalVariables.myobjects[0].addPiece(10, 19,3, 3);
            GlobalVariables.myobjects[0].addPiece(11, 20, 4, 3);
            GlobalVariables.myobjects[0].addName("background");

            GlobalVariables.myobjects[1] = new myObject();
            GlobalVariables.myobjects[1].addLocation(new Vector2Int(1, 0));
            GlobalVariables.myobjects[1].addLocation(new Vector2Int(3, 0));
            GlobalVariables.myobjects[1].addPiece(0, 2, 0, 0);
            GlobalVariables.myobjects[1].addPiece(1, 7, 0, 1);
            GlobalVariables.myobjects[1].addName("door");

            GlobalVariables.myobjects[2] = new myObject();
            GlobalVariables.myobjects[2].addLocation(new Vector2Int(1, 0));
            GlobalVariables.myobjects[2].addLocation(new Vector2Int(3, 0));
            GlobalVariables.myobjects[2].addPiece(0, 4, 0, 0);
            GlobalVariables.myobjects[2].addPiece(1, 9, 0, 1);
            GlobalVariables.myobjects[2].addName("princess");


            GlobalVariables.myobjects[3] = new myObject();
            GlobalVariables.myobjects[3].addLocation(new Vector2Int(2, 4));
            GlobalVariables.myobjects[3].addLocation(new Vector2Int(2, 5));
            GlobalVariables.myobjects[3].addPiece(0, 23, 0, 0);
            GlobalVariables.myobjects[3].addPiece(1, 24, 1, 0);
            GlobalVariables.myobjects[3].addPiece(2, 25, 2, 0);
            GlobalVariables.myobjects[3].addName("pond");

            GlobalVariables.myobjects[4] = new myObject();
            GlobalVariables.myobjects[4].addLocation(new Vector2Int(0, 3));
            GlobalVariables.myobjects[4].addPiece(0, 16, 0, 0);
            GlobalVariables.myobjects[4].addPiece(1, 17, 1, 0);
            GlobalVariables.myobjects[4].addName("well");

            GlobalVariables.myobjects[5] = new myObject();
            GlobalVariables.myobjects[5].addLocation(new Vector2Int(2, 2));
            GlobalVariables.myobjects[5].addLocation(new Vector2Int(1, 4));
            GlobalVariables.myobjects[5].addPiece(0, 13, 0, 0);
            GlobalVariables.myobjects[5].addName("earth");

            GlobalVariables.myobjects[6] = new myObject();
            GlobalVariables.myobjects[6].addLocation(new Vector2Int(1, 2));
            GlobalVariables.myobjects[6].addLocation(new Vector2Int(0, 4));
            GlobalVariables.myobjects[6].addPiece(0, 12, 0, 0);
            GlobalVariables.myobjects[6].addName("ball");

            GlobalVariables.myobjects[7] = new myObject();
            GlobalVariables.myobjects[7].addLocation(new Vector2Int(1, 2));
            GlobalVariables.myobjects[7].addLocation(new Vector2Int(0, 4));
            GlobalVariables.myobjects[7].addPiece(0, 21, 0, 0);
            GlobalVariables.myobjects[7].addName("potato");

            GlobalVariables.myobjects[8] = new myObject();
            GlobalVariables.myobjects[8].addLocation(new Vector2Int(2, 2));
            GlobalVariables.myobjects[8].addLocation(new Vector2Int(1, 4));
            GlobalVariables.myobjects[8].addPiece(0, 22, 0, 0);
            GlobalVariables.myobjects[8].addName("frog");

            GlobalVariables.myobjects[9] = new myObject();
            GlobalVariables.myobjects[9].addLocation(new Vector2Int(0,4));
            GlobalVariables.myobjects[9].addLocation(new Vector2Int(0, 5));
            GlobalVariables.myobjects[9].addPiece(0, 26, 0, 0);
            GlobalVariables.myobjects[9].addPiece(1, 27, 1, 0);
            GlobalVariables.myobjects[9].addPiece(2, 28, 2, 0);
            GlobalVariables.myobjects[9].addPiece(3, 29, 3, 0);
            GlobalVariables.myobjects[9].addPiece(4, 30, 4, 0);
            GlobalVariables.myobjects[9].addName("rail");
                           
            GlobalVariables.myobjects2 = new myObject[2];

            GlobalVariables.myobjects2[0] = new myObject();
            GlobalVariables.myobjects2[0].addPiece(0, 12,0,0,"Replace animations/BallorPotatoFalling12");
            GlobalVariables.myobjects2[0].addPiece(1, 16, -1, 1,"Replace animations/BallorPotatoFalling16");
            GlobalVariables.myobjects2[0].addPiece(2, 17, 0, 1,"Replace animations/BallorPotatoFalling17");

            GlobalVariables.myobjects2[1] = new myObject();
            GlobalVariables.myobjects2[1].addPiece(0, 21, 0, 0,"Replace animations/Potato12");
            GlobalVariables.myobjects2[1].addPiece(1, 16, -1, 1,"Replace animations/BallorPotatoFalling16");
            GlobalVariables.myobjects2[1].addPiece(2, 17, 0, 1,"Replace animations/BallorPotatoFalling17");

         



            Debug.Log("end intialization");

            //Debug.Log(GlobalVariables.getprogressToString());
            //Debug.Log(GlobalVariables.getStory(0, "2"));
            //Debug.Log(GlobalVariables.gametree1[",7,5,0"]);




        }
        else if (mapnum == 1)
        {
            GlobalVariables.myobjects = new myObject[1];
            GlobalVariables.myobjects[0] = new myObject();
            GlobalVariables.myobjects[0].addLocation(new Vector2Int(0, 0));
            GlobalVariables.myobjects[0].addPiece(0, 1, 0, 0);
            GlobalVariables.myobjects[0].addPiece(1, 2, 2, 0);
            GlobalVariables.myobjects[0].addPiece(2, 3, 4, 0);
            GlobalVariables.myobjects[0].addPiece(3, 4, 0, 1);
            GlobalVariables.myobjects[0].addPiece(4, 5, 2, 1);
            GlobalVariables.myobjects[0].addPiece(5, 6, 4, 1);
            GlobalVariables.myobjects[0].addPiece(6, 7, 0, 2);
            GlobalVariables.myobjects[0].addPiece(7, 8, 3, 2);
            GlobalVariables.myobjects[0].addPiece(8, 9, 4, 2);
            GlobalVariables.myobjects[0].addPiece(9, 10, 2, 3);
            GlobalVariables.myobjects[0].addPiece(10, 11, 3, 3);
            GlobalVariables.myobjects[0].addPiece(11, 12, 4, 3);
            GlobalVariables.myobjects[0].addPiece(12, 13, 0, 0);
            GlobalVariables.myobjects[0].addPiece(13, 14, 2, 0);
            GlobalVariables.myobjects[0].addPiece(14, 15, 4, 0);
            GlobalVariables.myobjects[0].addPiece(15, 16, 0, 1);
            GlobalVariables.myobjects[0].addPiece(16, 17, 2, 1);
            GlobalVariables.myobjects[0].addPiece(17, 18, 4, 1);
            GlobalVariables.myobjects[0].addPiece(18, 19, 0, 2);
            GlobalVariables.myobjects[0].addPiece(19, 20, 3, 2);
            GlobalVariables.myobjects[0].addPiece(20, 21, 4, 2);
            GlobalVariables.myobjects[0].addPiece(21, 22, 2, 3);
            GlobalVariables.myobjects[0].addPiece(22, 23, 3, 3);
            GlobalVariables.myobjects[0].addPiece(23, 24, 4, 3);
            GlobalVariables.myobjects[0].addPiece(24, 25, 3, 3);
            GlobalVariables.myobjects[0].addPiece(25, 26, 4, 3);
            GlobalVariables.myobjects[0].addPiece(26, 27, 2, 0);
            GlobalVariables.myobjects[0].addPiece(27, 28, 2, 0);
            GlobalVariables.myobjects[0].addPiece(28, 29, 4, 0);
            GlobalVariables.myobjects[0].addPiece(29, 30, 0, 1);
            GlobalVariables.myobjects[0].addName("puzzle2");

            //empty
            GlobalVariables.myobjects2 = new myObject[1];
            GlobalVariables.myobjects2[0] = new myObject();

            //GlobalVariables.myobjectsTwo = new myObject[1];
            //GlobalVariables.myobjectsTwo[0] = new myObject();
            //GlobalVariables.myobjectsTwo[0].addLocation(new Vector2Int(0, 0));
            //GlobalVariables.myobjectsTwo[0].addPiece(0, 1, 0, 0);
            //GlobalVariables.myobjectsTwo[0].addPiece(1, 2, 2, 0);
            //GlobalVariables.myobjectsTwo[0].addPiece(2, 3, 4, 0);
            //GlobalVariables.myobjectsTwo[0].addPiece(3, 4, 0, 1);
            //GlobalVariables.myobjectsTwo[0].addPiece(4, 5, 2, 1);
            //GlobalVariables.myobjectsTwo[0].addPiece(5, 6, 4, 1);
            //GlobalVariables.myobjectsTwo[0].addPiece(6, 7, 0, 2);
            //GlobalVariables.myobjectsTwo[0].addPiece(7, 8, 3, 2);
            //GlobalVariables.myobjectsTwo[0].addPiece(8, 9, 4, 2);
            //GlobalVariables.myobjectsTwo[0].addPiece(9, 10, 2, 3);
            //GlobalVariables.myobjectsTwo[0].addPiece(10, 11, 3, 3);
            //GlobalVariables.myobjectsTwo[0].addPiece(11, 12, 4, 3);
            //GlobalVariables.myobjectsTwo[0].addPiece(12, 13, 0, 0);
            //GlobalVariables.myobjectsTwo[0].addPiece(13, 14, 2, 0);
            //GlobalVariables.myobjectsTwo[0].addPiece(14, 15, 4, 0);
            //GlobalVariables.myobjectsTwo[0].addPiece(15, 16, 0, 1);
            //GlobalVariables.myobjectsTwo[0].addPiece(16, 17, 2, 1);
            //GlobalVariables.myobjectsTwo[0].addPiece(17, 18, 4, 1);
            //GlobalVariables.myobjectsTwo[0].addPiece(18, 19, 0, 2);
            //GlobalVariables.myobjectsTwo[0].addPiece(19, 20, 3, 2);
            //GlobalVariables.myobjectsTwo[0].addPiece(20, 21, 4, 2);
            //GlobalVariables.myobjectsTwo[0].addPiece(21, 22, 2, 3);
            //GlobalVariables.myobjectsTwo[0].addPiece(22, 23, 3, 3);
            //GlobalVariables.myobjectsTwo[0].addPiece(23, 24, 4, 3);
            //GlobalVariables.myobjectsTwo[0].addPiece(24, 25, 3, 3);
            //GlobalVariables.myobjectsTwo[0].addPiece(25, 26, 4, 3);
            //GlobalVariables.myobjectsTwo[0].addPiece(26, 27, 2, 0);
            //GlobalVariables.myobjectsTwo[0].addPiece(27, 28, 2, 0);
            //GlobalVariables.myobjectsTwo[0].addPiece(28, 29, 4, 0);
            //GlobalVariables.myobjectsTwo[0].addPiece(29, 30, 0, 1);
            //GlobalVariables.myobjectsTwo[0].addName("puzzle2");
        }

        
    }
	// Update is called once per frame
	void Update () {
		
	}
}
