﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PuzzleSelector : MonoBehaviour, IPointerClickHandler {

    public Sprite unlockSprite;
    private bool isUnlocked;

    private int loadingPuzzleNumber;

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        if (isUnlocked)
        {
            loadingPuzzleNumber = this.transform.GetSiblingIndex() + 1;
            Debug.Log(loadingPuzzleNumber);
            this.GetComponentInParent<GameMaster>().currentPuzzle = loadingPuzzleNumber;
            string loadingScene = "Puzzle" + loadingPuzzleNumber.ToString();
            //GlobalVariables.resetGrid();
            GlobalVariables.resetGame();
            SceneManager.LoadSceneAsync(loadingScene);
            print("puzzle number: " + this.GetComponentInParent<GameMaster>().currentPuzzle);
        }
    }

    public void UnlockPuzzle()
    {
        this.GetComponent<Image>().sprite = unlockSprite;
        isUnlocked = true;
    }

	// Use this for initialization
	void Start () {
        isUnlocked = false;
        if(this.transform.GetSiblingIndex() == 0)
        {
            isUnlocked = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
