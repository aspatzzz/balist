﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit
{
    public List<int> spritenums=new List<int>();
    public int unitid;

    public void addSpriteNum(int n){
        spritenums.Add(n);
    }

    public void addUnitID(int unitid){
        this.unitid = unitid;
    }
    public bool Move(int num, int oriposx, int oriposy, int newposx, int newposy)
    {
        //pre check
        HashSet<Vector2> unitposition = new HashSet<Vector2>();
        foreach (int s in spritenums)
        {
            Vector2Int currpos = GlobalVariables.pieceobjects[s - 1].GetComponent<PieceProperty>().pos;
            unitposition.Add(currpos);
        }
        foreach (int s in spritenums)
        {
            //need to check boundary TODO

            Vector2Int currpos = GlobalVariables.pieceobjects[s - 1].GetComponent<PieceProperty>().pos;
            Vector2Int movedpos = new Vector2Int(currpos.x + (newposx - oriposx), currpos.y + (newposy - oriposy));

            if (movedpos.x < 0 || movedpos.y < 0 || movedpos.x >= GlobalVariables.gridWidth || movedpos.y >= GlobalVariables.gridHeight||(GlobalVariables.grid[movedpos.x][movedpos.y].isOccupied&&!unitposition.Contains(movedpos)))
            {
                return false;
            }

        }
        foreach (int s in spritenums)
        {
            //need to check boundary TODO

            if (s != num)
            {
                int pieceid = s - 1;
                //Debug.Log("#7-2" + GlobalVariables.pieceobjects[6].GetComponent<PieceProperty>().pos);
                Vector2Int currpos = GlobalVariables.pieceobjects[s - 1].GetComponent<PieceProperty>().pos;
                //Debug.Log(currpos.x + "  currpost1 x y" + currpos.y);
                Vector2Int movedpos = new Vector2Int(currpos.x + (newposx - oriposx), currpos.y + (newposy - oriposy));
                //if (movedpos.x < 0 || movedpos.y < 0 ||movedpos.x>GlobalVariables.gridWidth||movedpos.y>GlobalVariables.gridHeight){
                //    return false;
                //}
                GlobalVariables.pieceobjects[s - 1].GetComponent<PieceProperty>().pos = movedpos;
                GlobalVariables.pieceobjects[s - 1].transform.position = GlobalVariables.calculateCoordinate(movedpos.x, movedpos.y);
                if (GlobalVariables.grid[currpos.x][currpos.y].pieceid == pieceid)
                {
                    GlobalVariables.grid[currpos.x][currpos.y].isOccupied = false;
                    GlobalVariables.grid[currpos.x][currpos.y].pieceid = -1;
                }
                GlobalVariables.grid[movedpos.x][movedpos.y].isOccupied = true;
                GlobalVariables.grid[movedpos.x][movedpos.y].pieceid = s - 1;

                Debug.Log(oriposx + " oripos x y" + oriposy + "newpos x" + newposx + " newpos y" + newposy + "s" + s);
                Debug.Log(movedpos.x + " move x y" + movedpos.y);
                Debug.Log(currpos.x + "  currpost2 x y" + currpos.y);
            }

        }
        return true;

    }
    public void tranformUnit(int num, float oriscreenposx, float oriscreenposy, float newscreenposx, float newscreenposy){
        foreach (int s in spritenums)
        {
            //need to check boundary TODO
            GameObject mygameobject = GlobalVariables.pieceobjects[s - 1];
            Vector2 currpos = mygameobject.transform.position;
            Vector2 movedpos = new Vector2(currpos.x + (newscreenposx - oriscreenposx), currpos.y + (newscreenposy - oriscreenposy));
                
            if (s != num)
            {
                mygameobject.transform.position = movedpos;
            }
        }
    }

    //void MoveOnDrag(int num, int oriposx, int oriposy, int newposx, int newposy)
    //{
    //    foreach (int s in spritenums)
    //    {
            
    //        //GlobalVariables.pieceobjects[s].transform.position = GlobalVariables.calculateCoordinate(movedpos.x, movedpos.y);

    //    }


    //}


    public void resetPos(){
        foreach (int s in spritenums)
        {
            //Debug.Log("reset unit");
            GameObject myobject = GlobalVariables.pieceobjects[s-1];
            //Debug.Log(myobject.GetComponent<PieceProperty>().pos+" "+s);
            myobject.transform.position = GlobalVariables.calculateCoordinate(myobject.GetComponent<PieceProperty>().pos.x, myobject.GetComponent<PieceProperty>().pos.y);

        }
        
    }
}
