﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
public class GlobalVariables {
    public static int gridWidth = 5;
    public static int gridHeight = 6;
    public static int numObjects = 5;
    public static int numPartMax = 5;
    public static GameObject mycanvas= GameObject.Find("Canvas").gameObject;
    public static Vector2 CanvasPos = new Vector2(mycanvas.transform.position.x,mycanvas.transform.position.y);//should not hardcode this
    public static Vector2 gridStart =new Vector2(-86, 135.5f) ;
    public static Vector2 textStart =  new Vector2(-440, 120);
    public static Vector2 buttonStart = new Vector2(0, -250);
    public static Vector2[] pieceCenters = initCenter(); 
    public static Cell[][] grid = initCell();
    public static GameObject[] pieceobjects = new GameObject[gridWidth * gridHeight];
    public static myObject[] myobjects;
    public static myObject[] myobjects2;
    public static myObject[] myobjectsTwo;
    public static List<Unit> units=new List<Unit>();//to be moved together
    public static float cellWidth = 43;
    public static float cellHeight= 54;
    public static  int[] storyProgress = new int[] {0,0,0};
    public static int currProgress = 0;

    public static Dictionary<string, int> gameProgress1 = new Dictionary<string, int>();
    public static Dictionary<string, int> gameProgress2 = new Dictionary<string, int>();

    public static int[] puzzleCompletion = new int[2];

    public static Dictionary<string, string > gametree1 = populateTree(0);
    public static Dictionary<string, string> gametree2 = populateTree(1);

    public static HashSet<int> unitIDSet = new HashSet<int>();
    public static bool debugMode = true;
    public static bool firstrun = true;

    public static bool isStoryValid;
    public static bool trueEndUnlocked = false;
    //public static Pieces[] pieceCollection = initPieceCollection();



    //screen space

    public static void resetTint(){
        for (int i = 0; i < GlobalVariables.myobjects.Length; i++)
        {
            setColorForObjNum(i, Color.white);
        }
    }
    public static void setColorForObjNum(int objnum, Color c)
    {

        for (int i = 0; i < GlobalVariables.pieceobjects.Length; i++)
        {
            if (GlobalVariables.pieceobjects[i])
            {
                int currid = GlobalVariables.pieceobjects[i].GetComponent<PieceProperty>().objid;
                if (currid == objnum)
                {
                    GlobalVariables.pieceobjects[i].GetComponent<Image>().color = c;
                }
            }
        }

    }
    public static void resetGame(){
        resetGrid();
        resetUnit();
        storyProgress = new int[] { 0, 0, 0 };
        currProgress = 0;
        firstrun = true;
        trueEndUnlocked = false;
     }
    public static void resetGrid(){
        grid = initCell();
    }
    public static void resetUnit(){
        unitIDSet = new HashSet<int>();
        units = new List<Unit>();
        resetTint();
        for (int i = 0; i < pieceobjects.Length; i++)
        {
            GlobalVariables.pieceobjects[i].GetComponent<Image>().material.SetFloat("_Tween", 0f);

        }

    }
    public static Vector2 calculateCoordinate(int i, int j)
    {
        //float xpos = GlobalVariables.gridStart.x + i * cellWidth;
        //float ypos = GlobalVariables.gridStart.y - j * cellHeight;
        //return new Vector2(xpos, ypos)+ GlobalVariables.CanvasPos;
        return grid[i][j].position;
    
    }
    public static Vector2[] initCenter(){
        Debug.Log(mycanvas.transform.localScale);
        Vector2[] temp=new Vector2[gridHeight*gridWidth];
        //for (int i = 0; i< gridWidth;i++){
        //    for (int j = 0; j< gridHeight; j++)
        //    {
        //        float scalex = mycanvas.transform.localScale.x;
        //        float scaley = mycanvas.transform.localScale.y;
        //        //bug?
        //        float xpos = GlobalVariables.gridStart.x + i * 49;
        //        float ypos = GlobalVariables.gridStart.y - j * 61;

        //        //Debug.Log(i * 4 + j);

        //        Vector2 sizeDelta = mycanvas.GetComponent<RectTransform>().sizeDelta;
        //        Vector2 canvasScale = new Vector2(mycanvas.transform.localScale.x, mycanvas.transform.localScale.y);

        //        Vector2 finalScale = new Vector2( canvasScale.x*sizeDelta.x, canvasScale.y*sizeDelta.y);

        //        Debug.Log(GlobalVariables.CanvasPos+"canvas position");
        //        ///GUI.Label(new Rect(0,0,0,1),"ok");

        //        temp[i * gridHeight + j] = Camera.main.WorldToScreenPoint(new Vector2(xpos,ypos));//new Vector2(xpos, ypos)+new Vector2(GlobalVariables.CanvasPos.x*finalScale.x,GlobalVariables.CanvasPos.y*finalScale.y);
        //        //temp.Rank
        //       // temp.add(new Vector2(xpos, ypos));
        //    }
        //}
        //Debug.Log(GlobalVariables.gridStart.x +mycanvas.transform.position.x);
        //Debug.Log(GlobalVariables.gridStart.y + mycanvas.transform.position.y);
        //Debug.Log(temp[0]+"upper left position");
        return temp;
    }
    public static Cell[][] initCell(){
        Cell[][] temp = new Cell[gridWidth][];
        for (int i = 0; i < gridWidth; i++)
        {
            temp[i] = new Cell[gridHeight];
        }
        for (int i = 0; i < gridWidth; i++)
        {
            for (int j = 0; j < gridHeight; j++)
            {
                //Debug.Log(temp[i][j].position);
                temp[i][j] = new Cell();
                temp[i][j].setPosition(pieceCenters[i * gridHeight + j]);
            }
        }
        return temp;
    }
    public static int getSceneNum(int storyno,string pieceorder){
        
        if(storyno==0){
            switch (pieceorder)
            {
                case "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30":
                    return 1;
                case "1,2,3,4,5,6,7,8,9,10,11,21,13,14,15,16,17,18,19,20,12,22,23,24,25,26,27,28,29,30":
                    Debug.Log("line 1");
                    return 2;
                case "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,26,27,28,29,30,21,22,23,24,25":
                    return 3;
                case "1,2,3,4,5,6,7,8,9,10,11,21,13,14,15,16,17,18,19,20,26,27,28,29,30,12,22,23,24,25":
                    return 4;
                case "1,2,3,4,5,6,7,8,9,10,11,12,22,14,15,16,17,18,19,20,21,13,23,24,25,26,27,28,29,30":
                    return 5;
                case "1,2,3,4,5,6,7,8,9,10,11,21,22,14,15,16,17,18,19,20,12,13,23,24,25,26,27,28,29,30":
                    return 6;
                case "1,4,3,2,5,6,9,8,7,10,11,12,13,14,15,16,17,18,19,20,26,27,28,29,30,21,22,23,24,25":
                    return 7;
                case "1,4,3,2,5,6,9,8,7,10,11,21,13,14,15,16,17,18,19,20,26,27,28,29,30,12,22,23,24,25":
                    return 8;
                case "1,4,3,2,5,6,9,8,7,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30":
                    return 9;
                case "1,4,3,2,5,6,9,8,7,10,11,21,13,14,15,16,17,18,19,20,12,22,23,24,25,26,27,28,29,30":
                    return 10;
                case "1,4,3,2,5,6,9,8,7,10,11,12,22,14,15,16,17,18,19,20,21,13,23,24,25,26,27,28,29,30":
                    return 11;
                case "1,4,3,2,5,6,9,8,7,10,11,21,22,14,15,16,17,18,19,20,12,13,23,24,25,26,27,28,29,30":
                    return 12;

               
                //case "1,2,3,4,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0":
                    //Debug.Log("for testing only");
                    //return 3;
                
            }
        }
        else if(storyno==1) {
            switch (pieceorder)
            {
                case "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30":
                    return 1;
                case "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,26,23,24,25,22,27,28,29,30":
                    return 2;
                case "1,2,3,4,5,6,7,8,9,10,11,26,13,14,23,16,17,18,19,20,21,22,15,24,25,12,27,28,29,30":
                    return 3;

                    //case "1,2,3,4,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0":
                    //Debug.Log("for testing only");
                    //return 3;

            }
        }
        return -1;
    }

    public static Dictionary<string, string> populateTree(int storyNum){
        Dictionary < string, string> newtree = new Dictionary<string, string>();

        TextAsset textFile = null;
        if (storyNum == 0)
        {
            textFile = Resources.Load("StoryLines") as TextAsset;
        }
        else if (storyNum == 1)
        {
            textFile = Resources.Load("StoryLines2") as TextAsset;
        }
        string[] linesInFile = textFile.text.Split('\n');
        //string itemStrings = reader.ReadLine();
        char[] delimiter = { ',' };

        foreach (string itemStrings in linesInFile)
        {
            if (itemStrings.Length > 1)
            {
                string[] parts = itemStrings.Split('@');
                string[] fields = parts[0].Split(delimiter);
                string[] filenames = parts[1].Split(delimiter);

                fields[0] = expandField(fields[0]);
                fields[1] = expandField(fields[1]);
                fields[2] = expandField(fields[2]);


                string key0 = "";
                string key1 = "";
                string key2 = "";
                for (int i = 0; i < fields[0].Split('#').Length; i++)
                {
                    for (int j = 0; j < fields[1].Split('#').Length; j++)
                    {
                        for (int k = 0; k < fields[2].Split('#').Length; k++)
                        {
                            key0 = "," + fields[0].Split('#')[i].Trim() + ",0,0";
                            key1 = "," + fields[0].Split('#')[i].Trim() + "," + fields[1].Split('#')[j].Trim() + ",0";
                            key2 = "," + fields[0].Split('#')[i].Trim() + "," + fields[1].Split('#')[j].Trim() + "," + fields[2].Split('#')[k].Trim();
                            if (!newtree.ContainsKey(key0))
                            {
                                newtree.Add(key0, filenames[0]);
                                if (storyNum == 0)
                                {
                                    gameProgress1.Add(key0, 0);
                                }
                                else
                                {
                                    gameProgress2.Add(key0, 0);
                                }
                                
                            }
                            if (!newtree.ContainsKey(key1))
                            {
                                newtree.Add(key1, filenames[1]);
                                if (storyNum == 0)
                                {
                                    gameProgress1.Add(key1, 0);
                                }
                                else
                                {
                                    gameProgress2.Add(key1, 0);
                                }
                            }
                            if (!newtree.ContainsKey(key2))
                            {
                                newtree.Add(key2, filenames[2]);
                                if (storyNum == 0)
                                {
                                    gameProgress1.Add(key2, 0);
                                }
                                else
                                {
                                    gameProgress2.Add(key2, 0);
                                }
                            }
                        }
                    }
                }

                //Debug.Log(key0+"kkey0");

                //itemStrings = reader.ReadLine();
                //Debug.Log(newtree[key0]);
            }
        }
       
        return newtree;
    }

    public static string readFile(string filestem)
    {
        //Dictionary<string, string> newtree = new Dictionary<string, string>();

        //StreamReader reader = new StreamReader("./Assets/Resources/Story/"+filestem+".txt");

        //string itemStrings = reader.ReadLine();
        //string total = "";
        //while (itemStrings != null && itemStrings.Length > 1)
        //{
        //    total += itemStrings;
        //    itemStrings = reader.ReadLine();
        //}
        //return total;

        string path = "Story/" + filestem;
        TextAsset textAsset = Resources.Load(path) as TextAsset;
        return textAsset.text;
            
    }
    public static string expandField(string field){
        string temp = "";
        if (!field.Contains("-"))
        {
            return field.Replace('{', ' ').Replace('}', ' ').Trim();
        }
        else{
            field = field.Replace('{', ' ').Replace('}', ' ');
            string[] parts = field.Substring(1,field.Length-2).Split('-');
            //Debug.Log(parts[0]);
            //Debug.Log(parts[1]);
            int start = int.Parse(parts[0]);
            int end = int.Parse(parts[1]);
            for (int i = start; i <= end; i++)
            {
                temp = temp + "#" + i;
             }
        }

        return temp.Substring(1).Trim();


    }
    public static string getprogressToString(){
        string temp = "";
        for (int i = 0; i < storyProgress.Length;i++){
            temp = temp + "," + storyProgress[i];
        }
        return temp;
    }
    public static string getStory(int storyno,string pieceorder){
        string storystring="";
        if (storyno == 0)
        {
            int scenenum = getSceneNum(storyno, pieceorder);
            if (scenenum >= 1)
            {
                Debug.Log("valid solution");
                storyProgress[currProgress] = scenenum;
                //storyno = 0;
                if (gametree1.ContainsKey(getprogressToString()))
                {
                    storystring = gametree1[getprogressToString()];
                    currProgress += 1;
                }

                if (currProgress == 3)
                {
                    gameProgress1[getprogressToString()] = 1;
                }
            }
            else{
                Debug.Log("incomplete puzzle"+scenenum+pieceorder);
            }
        }
        else if (storyno == 1)
        {
            int scenenum = getSceneNum(storyno, pieceorder);
            if (scenenum >= 1)
            {
                Debug.Log("valid solution");
                Debug.Log(getSceneNum(storyno, pieceorder));
                Debug.Log(gametree2[",1,0,0"]);
                Debug.Log(getprogressToString());
                storyProgress[currProgress] = scenenum;
                //storyno = 0;
                if (gametree2.ContainsKey(getprogressToString()))
                {
                    storystring = gametree2[getprogressToString()];
                    currProgress += 1;
                }

                if (currProgress == 3)
                {
                    gameProgress2[getprogressToString()] = 1;
                }
            }
            else
            {
                Debug.Log(getSceneNum(storyno, pieceorder));
                Debug.Log(gametree2[",1,0,0"]);
                Debug.Log("incomplete puzzle");
            }
        }

        if (storystring.Trim().Length == 0)
        {
            storystring = "0";
            isStoryValid = false;
        }
        else
        {
            isStoryValid = true;
        }

        storystring = (storyno+1).ToString() + "/" + storystring.Trim();
        
        return storystring;


    }
    //public static Pieces[] initPieceCollection(){
    //    return null;
    //}
}
