﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuController : MonoBehaviour {

    private GameObject mainMenu;
    private GameObject helpMenu;
    private bool isPauseMenuOn;

	// Use this for initialization
	void Start ()
    {
        mainMenu = transform.Find("MainMenu").gameObject;
        helpMenu = mainMenu.transform.Find("HelpMenu").gameObject;
        isPauseMenuOn = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
	}

    void Pause()
    {
        isPauseMenuOn = !isPauseMenuOn;
        mainMenu.SetActive(isPauseMenuOn);
        if (isPauseMenuOn)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void Resume()
    {
        isPauseMenuOn = false;
        mainMenu.SetActive(isPauseMenuOn);
        Time.timeScale = 1;
    }

    public void HelpMenuControl(bool ifOpen)
    {
        helpMenu.SetActive(ifOpen);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
