﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PuzzleConfirm : MonoBehaviour {

    Button myButton;
    public GameObject scrollText;
    public GameObject chapterTransition;
    public GameObject tutorialCanvas;
    public GameObject puzzleProcess;

    private GameObject storyCanvas;
    public GameObject imagepiece;
    private Text textBoxOne;
    private Text textBoxTwo;

    //post-ciga //
    public Sprite Continue;
    public Sprite Sumbit;
    public bool WaitForContinue = false;

    void Awake()
    {
        myButton = GetComponent<Button>(); // <-- you get access to the button component here

        myButton.onClick.AddListener(() => { ConfirmPuzzle(); });  // <-- you assign a method to the button OnClick event here

        storyCanvas = GameObject.Find("StoryCanvas").gameObject; // find story canvas to access textbox
        textBoxOne = storyCanvas.transform.Find("Textbox").GetChild(0).GetComponent<Text>();
        textBoxTwo = storyCanvas.transform.Find("Textbox").GetChild(1).GetComponent<Text>();

        // initialize textBox contents
        //textBoxOne.text = "";
        //textBoxTwo.text = "";
    }

    void reArrange()
    {
        
    }
    int getnumObjects(){
        int maxid = -1;
        for (int i = 0; i < GlobalVariables.pieceobjects.Length; i++)
        {
            if (GlobalVariables.pieceobjects[i])
            {
                int currid = GlobalVariables.pieceobjects[i].GetComponent<PieceProperty>().objid;
                if (currid > maxid)
                {
                    maxid = currid;
                }
            }

        }
        return maxid+1;
        
    }
    public void ConfirmPuzzle(){
        //Debug.Log("what???" + myButton.onClick.GetPersistentMethodName(0));
        if (!WaitForContinue)
        {
            CheckPuzzle(-1);
            checkEnd();
            GlobalVariables.resetTint();
        }
    }
    void checkEnd(){
        GlobalVariables.resetUnit();
       
        string storystring = "";
        for (int i = 0; i < GlobalVariables.gridHeight; i++)
        {
            for (int j = 0; j < GlobalVariables.gridWidth; j++)
            {
                Cell piececell = GlobalVariables.grid[j][i];
                if (storystring.Length > 0)
                {
                    storystring = storystring + "," + (piececell.pieceid + 1);
                }
                else{
                    storystring =""+ (piececell.pieceid + 1);
                }



            }

        }


        int storyno = GameObject.Find("MenuCanvas").GetComponent<GameMaster>().currentPuzzle - 1;
        string fileStem = GlobalVariables.getStory(storyno, storystring);
        //TODO
        if(fileStem.Length>0&&fileStem.Split('/')[1]!="0"){
            //GlobalVariables.resetTint();
            GlobalVariables.resetUnit();
        }

        // if got Correct Ending, then unlock next puzzle
        // TODO: no hard code
        if (fileStem.Equals("1/1c1"))
        {
            GlobalVariables.trueEndUnlocked = true;
            GameObject.Find("PuzzleSelectionHolder").GetComponent<Animator>().SetBool("PuzzleSelectionMenuOn", true);
            StartCoroutine(WaitMenuOpen(1));
        }
        if (fileStem.Equals("2/1c1"))
        {
            GlobalVariables.trueEndUnlocked = true;
            GameObject.Find("PuzzleSelectionHolder").GetComponent<Animator>().SetBool("PuzzleSelectionMenuOn", true);
            StartCoroutine(WaitMenuOpen(2));
        }

        if (GlobalVariables.isStoryValid)
        {
            //post-ciga
            myButton.GetComponent<Image>().sprite = Continue;
            WaitForContinue = true;
            myButton.onClick.RemoveListener(() => { ConfirmPuzzle(); });
            myButton.onClick.AddListener(() => { CheckContinue(); });

            if (GlobalVariables.trueEndUnlocked)
            {
                chapterTransition.GetComponent<TransitionController>().unlockTrueEndTransition();
            }
            chapterTransition.SetActive(true);
            StartCoroutine(WaitTransition());
        }
        print("file: " + fileStem);

        string actualtext = GlobalVariables.readFile(fileStem);
        //TODO somehow the text is not showing up until after restarting.

        // if text is too long, then split into two halves, each is displayed in one textbox
        if (actualtext.Length > 100)
        {
            int halfIndex = actualtext.Length / 2;
            textBoxOne.text = actualtext.Substring(0, halfIndex);
            textBoxTwo.text = actualtext.Substring(halfIndex);
        }
        else
        {
            textBoxOne.text = actualtext;
            textBoxTwo.text = "";
        }

        //scrollText.gameObject.GetComponentInChildren<ScrollRect>().content.GetComponent<Text>().text = "cofnirmed";
        //Debug.Log(GlobalVariables.getStory(0, storystring));
        Debug.Log(actualtext);
        Debug.Log(storystring);
    }

    IEnumerator WaitMenuOpen(int puzzleChildNum)
    {
        yield return new WaitForSeconds(1);
        GameObject.Find("PuzzlesHolder").transform.GetChild(puzzleChildNum).GetComponent<PuzzleSelector>().UnlockPuzzle();
    }

    IEnumerator WaitTransition()
    {
        yield return new WaitForSeconds(3);
        chapterTransition.GetComponent<TransitionController>().incrementChapter();
        chapterTransition.SetActive(false);

        if (GameObject.Find("MenuCanvas").GetComponent<GameMaster>().ifTutorial)
        {
            tutorialCanvas.SetActive(true);
            GameObject.Find("MenuCanvas").GetComponent<GameMaster>().ifTutorial = false;

            yield return new WaitForSeconds(tutorialCanvas.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
            tutorialCanvas.SetActive(false);
        }
    }

    //public void UnlockSetup(int storyno){

    //    GameObject canvas = GameObject.Find("Canvas").gameObject;
    //    GameObject[] pieceobjects = new GameObject[GlobalVariables.gridWidth * GlobalVariables.gridHeight];
    //    for (int i = 0; i < GlobalVariables.gridHeight; i++)
    //    {
    //        for (int j = 0; j < GlobalVariables.gridWidth; j++)
    //        {
    //            int num = i * 5 + j + 1;
    //            var createImage = Instantiate(imagepiece, new Vector2(200, 120) - new Vector2(-j * GlobalVariables.cellWidth, i * GlobalVariables.cellHeight), transform.rotation) as GameObject;
    //            createImage.transform.SetParent(canvas.transform, false);
    //            if (num < 10)
    //            {
    //                createImage.gameObject.GetComponent<Image>().sprite = Resources.Load("Puzzle2/images/image_part_00" + num, typeof(Sprite)) as Sprite;
    //            }
    //            else
    //            {
    //                createImage.gameObject.GetComponent<Image>().sprite = Resources.Load("Puzzle2/images/image_part_0" + num, typeof(Sprite)) as Sprite;

    //            }
    //            createImage.gameObject.GetComponent<PieceProperty>().id = num - 1;
    //            //createImage.gameObject.GetComponent<PieceProperty>().setIds(1, 0, 0, 0);
    //            setIDfromObject(createImage, num);
    //            GlobalVariables.pieceobjects[num - 1] = createImage.gameObject;



    //        }
    //    }
        
    //}
    ////TO MOVE
    //public void setIDfromObject(GameObject createImage, int spritenum)
    //{
        
    //    for (int i = 0; i < GlobalVariables.myobjectsTwo.Length; i++)
    //    {
    //        myObject currObject = GlobalVariables.myobjectsTwo[i];

    //        if (currObject != null && currObject.pieceList.ContainsKey(spritenum))
    //        {
    //            createImage.gameObject.GetComponent<PieceProperty>().setIds(i, currObject.piecepartnumList[spritenum], currObject.pieceList[spritenum].x, currObject.pieceList[spritenum].y);
    //            //Debug.Log(i+" "+ currObject.piecepartnumList[spritenum]+" "+ currObject.pieceList[spritenum].x+" "+currObject.pieceList[spritenum].y);
    //        }
    //    }

    //}
    void CheckAnimation(myObject[] myobjects,int recentpieceid)
    {
        Debug.Log("checkAnimation"+myobjects.Length);
        Vector2[][] objpositions = new Vector2[myobjects.Length][];
        Vector2[][] relpositions = new Vector2[myobjects.Length][];
       

        for (int i = 0; i < myobjects.Length; i++)
        {
            objpositions[i] = new Vector2[myobjects[i].pieces.Count];
            relpositions[i] = new Vector2[myobjects[i].pieces.Count];
            for (int j = 0; j < myobjects[i].pieces.Count; j++)
            {
                objpositions[i][j] = new Vector2(-1, -1);
                relpositions[i][j] = new Vector2(-1, -1);

            }
        }

        for (int i = 0; i < myobjects.Length; i++)
        {
            for (int j = 0; j < myobjects[i].pieces.Count; j++)
            {
                //Debug.Log(myobjects[i].pieces[0]);
                if (GlobalVariables.pieceobjects[myobjects[i].pieces[j]-1].GetComponent<PieceProperty>())
                {
                    objpositions[i][j] = GlobalVariables.pieceobjects[myobjects[i].pieces[j] - 1].GetComponent<PieceProperty>().pos;
                    relpositions[i][j] = myobjects[i].relpos[j];
                    //Debug.Log("curr pos"+objpositions[i][j] + "----obj:" + i + "part:"+j);
                    //Debug.Log("rel pos"+relpositions[i][j] + "----obj:" + i + "part:"+j);
                }

               // Debug.Log(objpositions[i][j]);
            }
        }



        for (int objnum = 0; objnum < objpositions.Length; objnum++)
        {

            bool objFound = true;
            bool objPositionCorrect = false;
            bool hasPart = false;
            for (int partnum = 0; partnum < relpositions[objnum].Length; partnum++)
            {
                if (objpositions[objnum][partnum].x >= 0)
                {
                    hasPart = true;

                    Vector2 myoriginalposition = new Vector2(objpositions[objnum][0].x, objpositions[objnum][0].y);
                    Vector2 myshiftedposition = objpositions[objnum][partnum] - objpositions[objnum][0];
                    Vector2 correctposition = relpositions[objnum][partnum];
                    //Debug.Log("object:" + objnum + "partnum:" + partnum);
                    //Debug.Log("object position:"+objpositions[objnum][partnum]+"shifted position"+myshiftedposition + "correct position" + correctposition+"at0"+objpositions[objnum][0]);
                    if ((myshiftedposition - correctposition).magnitude >= 0.1)
                    {
                    ////   Debug.Log( "false");
                        objFound = false;
                    }

        
                }
                else
                {
                   //// Debug.Log(objnum + "justfalse" + partnum);
                    objFound = false;
                }


            }

            if (hasPart&&objFound)
            {
            ///blue
                 setColorForObjNum2(myobjects,objnum, Color.cyan);

                int unitid = -objnum - 1;//map to negative number to avoid collision with the other object set
                if (!GlobalVariables.unitIDSet.Contains(unitid) && relpositions[objnum].Length > 1)//&&GlobalVariables.myobjects[objnum].pieces.contains(mostrecentlymovedpieceidthatispassedinasparameter)
                {
                    Unit newunit = new Unit();

                    Dictionary<int, int> piecelist = myobjects[objnum].pieces;

                    //remove old
                    Unit temp=null;
                    int tempunitid = int.MaxValue;
                    foreach(Unit u in GlobalVariables.units){
                        foreach(int pid in piecelist.Values){
                            //Debug.Log(pid);
                            if (u.spritenums.Contains(pid))
                            {
                                
                                temp = u;
                                tempunitid = u.unitid;

                            }
                        }

                    }
                    if (temp!=null)
                    {
                        GlobalVariables.units.Remove(temp);
                        GlobalVariables.unitIDSet.Remove(tempunitid);

                    }
                    //Debug.Log(tempunitid + "tempunitid");
                    //add new

                    foreach (KeyValuePair<int, int> keyandspritenum in piecelist)
                    {
                        int spritenum = keyandspritenum.Value;
                        newunit.addSpriteNum(spritenum);
                    }
                    newunit.addUnitID(unitid);
                    //TODO
                    GlobalVariables.units.Add(newunit);
                    Debug.Log("new unit added" + newunit.spritenums.Count);
                    GlobalVariables.unitIDSet.Add(unitid);

                    animatepiece(myobjects, objnum);
                }
                //start animation;
            }
        }
        Debug.Log(GlobalVariables.units.Count+"unit count");
       
        Debug.Log(GlobalVariables.unitIDSet.Count + "unit count");
        foreach(int u in GlobalVariables.unitIDSet){
            
            Debug.Log(u + "&&&&");
        }
    }
    public void animatepiece(myObject[] myobjects,int objnum){
        myObject myobject = myobjects[objnum];
        foreach(KeyValuePair<int, string> spritenum_imagename in myobject.spriteList){
            int spritenum=spritenum_imagename.Key;
            string spritename = spritenum_imagename.Value;
            Sprite myspirte2 = Resources.Load(spritename, typeof(Sprite)) as Sprite;
            GlobalVariables.pieceobjects[spritenum - 1].GetComponent<Image>().material.SetTexture("_SecondTex", myspirte2.texture);

            // Added lerp on setFloat to mimick animation
            StartCoroutine(SpriteAnim(spritenum));
        }


    }

    IEnumerator SpriteAnim(int spritenum)
    {
        float lerpDuration = 3f;
        for (float t = 0f; t < lerpDuration; t += Time.deltaTime)
        {
            GlobalVariables.pieceobjects[spritenum - 1].GetComponent<Image>().material.SetFloat("_Tween", Mathf.Lerp(0, 1f, t/ lerpDuration));
            yield return null;
        }
    }

    public void CheckPuzzle(int recentpieceid){
        myObject[] myObjects = GlobalVariables.myobjects;
        Debug.Log("checkPuzzle");

        int maxObject = getnumObjects();
        Vector2[][] objpositions = new Vector2[maxObject][];
        Vector2[][] relpositions = new Vector2[maxObject][];
        int[] maxParts = new int[maxObject];

        for (int i = 0; i < GlobalVariables.pieceobjects.Length; i++)
        {
            if (GlobalVariables.pieceobjects[i])
            {
                int currid = GlobalVariables.pieceobjects[i].GetComponent<PieceProperty>().objid;
                int currpartid = GlobalVariables.pieceobjects[i].GetComponent<PieceProperty>().partnum;

                if (maxParts[currid] < currpartid)
                {
                    maxParts[currid] = currpartid;
                }
            }

        }
        for (int i = 0; i < maxObject; i++)
        {
            
                    
            objpositions[i] = new Vector2[maxParts[i]+1];
            relpositions[i] = new Vector2[maxParts[i]+1];
            for (int j = 0; j < maxParts[i] + 1; j++){
                objpositions[i][j] = new Vector2(-1, -1);
                relpositions[i][j] = new Vector2(-1, -1);

            }
        }
        for (int i = 0; i < GlobalVariables.gridWidth; i++)
        {
            for (int j = 0; j < GlobalVariables.gridHeight; j++)
            {
                Cell piececell = GlobalVariables.grid[i][j];

                if (piececell.isOccupied&&piececell.pieceid>=0)
                {
                    //Debug.Log(piececell.pieceid);
                    PieceProperty pieceProperty = GlobalVariables.pieceobjects[piececell.pieceid].GetComponent<PieceProperty>();
                    objpositions[pieceProperty.objid][pieceProperty.partnum] = new Vector2(i, j);
                    relpositions[pieceProperty.objid][pieceProperty.partnum] = pieceProperty.partRelativePos;
                }
                 
            }
        }

        for (int objnum = 0; objnum < objpositions.Length;objnum++){

            bool objFound = true;
            bool objPositionCorrect = false;
            bool hasPart = false;
            for (int partnum = 0; partnum < relpositions[objnum].Length; partnum++)
            {
                if (objpositions[objnum][partnum].x >= 0)
                {
                    hasPart = true;
                    //Debug.Log(objnum + "objnum" + partnum);
                    Vector2Int myoriginalposition = new Vector2Int(Mathf.RoundToInt(objpositions[objnum][0].x),Mathf.RoundToInt(objpositions[objnum][0].y));
                    Vector2 myshiftedposition = objpositions[objnum][partnum] - objpositions[objnum][0];
                    Vector2 correctposition = relpositions[objnum][partnum];
                    //Debug.Log(objpositions[objnum][partnum]);
                    //Debug.Log(correctposition);
                    //Debug.Log(GlobalVariables.myobjects[objnum].possibleLocations);
                    if ((myshiftedposition - correctposition).magnitude >= 0.1)
                    {
                        objFound = false;
                    }

                    if(GlobalVariables.myobjects[objnum].possibleLocations.Contains(myoriginalposition)){
                        objPositionCorrect = true;
                    }
                }
                else{
                    objFound = false;
                }
                    

            }

            if (hasPart)
            {
                
                if (GlobalVariables.myobjects[objnum].isunit&&GlobalVariables.myobjects[objnum].pieces.ContainsValue(recentpieceid + 1))
                {
                    if (objFound)
                    {
                        int unitid = objnum;
                        Dictionary<int, int> piecelist = GlobalVariables.myobjects[objnum].pieces;

                        bool hadpieceinunit = false;
                        foreach (Unit u in GlobalVariables.units)
                        {
                            foreach (int pid in piecelist.Values)
                            {
                                if (u.spritenums.Contains(pid))
                                {
                                    hadpieceinunit = true;
                                }
                            }

                        }
                        if (!hadpieceinunit&&!GlobalVariables.unitIDSet.Contains(objnum)&&relpositions[objnum].Length>1)//&&GlobalVariables.myobjects[objnum].pieces.contains(mostrecentlymovedpieceidthatispassedinasparameter)
                        {
                            Unit newunit = new Unit();




                            foreach (KeyValuePair<int, int> keyandspritenum in piecelist)
                            {
                                int spritenum = keyandspritenum.Value;
                                newunit.addSpriteNum(spritenum);
                            }
                            newunit.addUnitID(unitid);
                            GlobalVariables.units.Add(newunit);
                            Debug.Log("new unit added" + newunit.spritenums.Count);

                            GlobalVariables.unitIDSet.Add(unitid);
                        }
                        setColorForObjNum(objnum, Color.yellow);
                        if (objPositionCorrect)
                        {
                            setColorForObjNum(objnum, Color.yellow);
                        }

                    }
                    else
                    {
                        setColorForObjNum(objnum, Color.white);
                    }
                }

                //if(GlobalVariables.myobjects[objnum].triggersAnimation&&objFound){
                //    Debug.Log(objnum+"triggerAnimation");

                //    setColorForObjNum(objnum, Color.blue);
                //}


            }
        }
       
    
         CheckAnimation(GlobalVariables.myobjects2,recentpieceid);
    }
    public void setColorForObjNum(int objnum,Color c){

        for (int i = 0; i < GlobalVariables.pieceobjects.Length; i++)
        {
            if (GlobalVariables.pieceobjects[i])
            {
                int currid = GlobalVariables.pieceobjects[i].GetComponent<PieceProperty>().objid;
                if(currid==objnum){
                    GlobalVariables.pieceobjects[i].GetComponent<Image>().color = c;
                }
            }
        }

    }

    public void setColorForObjNum2(myObject[] myobjects,int objnum, Color c)
    {

   
            for (int j = 0; j < myobjects[objnum].pieces.Count; j++)
            {
                //Debug.Log(myobjects[i].pieces[0]);
           
                GlobalVariables.pieceobjects[myobjects[objnum].pieces[j] - 1].GetComponent<Image>().color = c;

            }




    }
    public int getObjNum(GameObject g){
        return g.GetComponent<PieceProperty>().objid;
        
    }
   
    //post-ciga
    public void CheckContinue()
    {
        Debug.Log("times");
        WaitForContinue = false;
        myButton.GetComponent<Image>().sprite = Sumbit;
        myButton.onClick.RemoveAllListeners();
        myButton.onClick.AddListener(() => { ConfirmPuzzle(); });

        // Reset Game if finished a storyline but has not unlocked the true end
        Debug.Log("true end: " + GlobalVariables.trueEndUnlocked);

        if (GlobalVariables.currProgress == 3)
        {
            puzzleProcess.GetComponent<PuzzleProcessController>().setPuzzleProcess(GameObject.Find("MenuCanvas").GetComponent<GameMaster>().currentPuzzle - 1);
            
        }

        if(GlobalVariables.currProgress == 3 && !GlobalVariables.trueEndUnlocked)
        {
            Scene activeScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(activeScene.name);
            GlobalVariables.resetGame();
        }

    }
}
