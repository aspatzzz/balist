﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSelectionMenuController : MonoBehaviour {

    private Animator puzzleSelectionMenuAnimator;
    private bool puzzleSelectionMenuOn;

    public GameObject puzzleElement;
    public Transform puzzlesHolder;

	// Use this for initialization
	void Start () {
        puzzleSelectionMenuAnimator = this.GetComponent<Animator>();
        puzzleSelectionMenuOn = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.A))
        {
            AddNewPuzzleElement();
        }
	}

    public void SelectionMenuStatus()
    {
        puzzleSelectionMenuOn = !puzzleSelectionMenuOn;
        puzzleSelectionMenuAnimator.SetBool("PuzzleSelectionMenuOn", puzzleSelectionMenuOn);
    }

    public void AddNewPuzzleElement()
    {
        puzzleElement = Instantiate(puzzleElement) as GameObject;
        puzzleElement.transform.parent = puzzlesHolder;

    }
}
