﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myObjectGroup {

    //not in use
    public Dictionary<int, Vector2> relpos= new Dictionary<int, Vector2>();
    public Dictionary<int, int> pieces = new Dictionary<int, int>();
    public string myname;
    public Dictionary<int, Vector2Int> pieceList = new Dictionary<int, Vector2Int>();
    public Dictionary<int, int> piecepartnumList = new Dictionary<int, int>();
    public Dictionary<int, string> spriteList = new Dictionary<int, string>();
    bool triggersAnimation=true;
    Shader animationShader;
    bool snaps=true;
   

    public void addPiece(int partnum, int spritenum, Vector2Int relpos,string spriteName)
    {
        this.relpos.Add(partnum, relpos);
        this.pieces.Add(partnum, spritenum);
        this.pieceList.Add(spritenum, relpos);
        this.piecepartnumList.Add(spritenum, partnum);
        this.spriteList.Add(spritenum, spriteName);
    }

    public void addPiece(int partnum, int spritenum, int relposx, int relposy,string spriteName)
    {
        this.relpos.Add(partnum, new Vector2Int(relposx, relposy));
        this.pieces.Add(partnum, spritenum);
        this.pieceList.Add(spritenum, new Vector2Int(relposx, relposy));
        this.piecepartnumList.Add(spritenum, partnum);
        this.spriteList.Add(spritenum, spriteName);


    }
    public void addName(string n)
    {
        this.myname = n;
    }

}
