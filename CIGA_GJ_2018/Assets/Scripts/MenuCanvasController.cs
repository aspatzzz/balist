﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCanvasController : MonoBehaviour {

    private static MenuCanvasController menuCanvasInstance;

    private void Awake()
    {
        DontDestroyOnLoad(this);

        if (menuCanvasInstance == null)
        {
            menuCanvasInstance = this;
        }
        else
        {
            DestroyObject(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
